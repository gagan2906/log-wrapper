package log

import "github.com/sirupsen/logrus"

type Config struct {
	logFileName string
	level       logrus.Level
	printCaller bool
	logType		string
}

func NewLogConfig(logFileName string, logLevel string, printCaller bool, logType string) *Config {
	var level logrus.Level
	switch logLevel {
	case "INFO":
		level = logrus.InfoLevel
	case "DEBUG":
		level = logrus.DebugLevel
	case "ERROR":
		level = logrus.ErrorLevel
	case "FATAL":
		level = logrus.FatalLevel
	case "TRACE":
		level = logrus.TraceLevel
	default:
		level = logrus.InfoLevel
	}

	return &Config{logFileName: logFileName, level: level, printCaller: printCaller, logType: logType}
}