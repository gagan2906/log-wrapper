package log

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"os"
	"runtime"
)

//take file name from context and initialize the logger from the file name
var logger = logrus.New()
var mdc = make(map[string]interface{})
var printCaller = false
var loggerInitialized = false

func Init(config Config) {
	if loggerInitialized {
		return
	}
	printCaller = config.printCaller
	logger.SetLevel(config.level)
	switch config.logType {
	case "JSON":
		logger.SetFormatter(&logrus.JSONFormatter{})
	default:
		logger.SetFormatter(&logrus.TextFormatter{})
	}
	err := setLogFileName(config.logFileName)
	if err != nil {
		fmt.Println("Not able to open log file: %s, cause: %v", config.logFileName, err)
		panic("Not able to initialize logger.")
	}
	loggerInitialized = true
}

func setLogFileName(filename string) error {
	f, err := os.OpenFile(filename, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0755)
	if err != nil {
		fmt.Errorf("not able to open log file for writing. %v", err)
		return err
	}
	logger.SetOutput(f)
	return nil
}

func Info(args ...interface{}) {
	name, ok := getCallerName()

	switch {
	case len(mdc) == 0 && !ok:
		logger.Info(args...)
	case len(mdc) == 0 && ok:
		logger.WithField("callerName", name).Info(args...)
	default:
		logger.WithField("callerName", name).WithFields(getFields()).Info(args...)
	}
}

func Debug(args ...interface{}) {
	name, ok := getCallerName()

	switch {
	case len(mdc) == 0 && !ok:
		logger.Debug(args...)
	case len(mdc) == 0 && ok:
		logger.WithField("callerName", name).Debug(args...)
	default:
		logger.WithField("callerName", name).WithFields(getFields()).Debug(args...)
	}
}

func Error(args ...interface{}) {
	name, ok := getCallerName()

	switch {
	case len(mdc) == 0 && !ok:
		logger.Error(args...)
	case len(mdc) == 0 && ok:
		logger.WithField("callerName", name).Error(args...)
	default:
		logger.WithField("callerName", name).WithFields(getFields()).Error(args...)
	}
}

func getCallerName() (string, bool) {
	if !printCaller {
		return "", false
	}
	pc, _, _, ok := runtime.Caller(2)
	details := runtime.FuncForPC(pc)
	if ok && details != nil {
		return details.Name(), ok
	}
	return "", ok
}


func getFields() logrus.Fields {
	fields := logrus.Fields{}
	for key, val := range mdc {
		fields[key] = val
	}
	return fields
}

// Sets the key-value in the mdc of the log
func MDCPut(key string, value interface{}) {
	mdc[key] = value
}

// Clears the key-value in the mdc of the log
func ClearMDC() {
	mdc = make(map[string]interface{})
}

//Removes a single key from mdc
func MDCRemove(key string){
	delete(mdc, key)
}