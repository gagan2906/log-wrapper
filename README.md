# README #


### What is this repository for? ###

* log-wrapper repository aims to wraps logging functionality that can be from any logger
* v1.0.0

### How do I get set up? ###
Just run the following, assuming `golang` is installed your `GOPATH` is already set and

    go get gitlab.com/gagan2906/log-wrapper
or add the required package in `go.mod` or `Gopkg.toml` 

### Contribution guidelines ###

* Use `go fmt` before committing

### Whom can I contact for improvements? ###

* Gagan Chouhan (gagan2906@gmail.com)
* LinkedIn (www.linkedin.com/in/gagan-chouhan-nitb)